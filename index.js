// While loop
/*
	-Evaluate a certain condition. If it satisfied, it will repeat codes continously.
	-Syntax
		while(expression/condition){
			statement/s;
		}
*/

let count = 5;

while(count !==0 ){
	console.log("While: " + count);
	count--;
}
/*
	Expected output:
	While: 5
	While: 4
	While: 3
	While: 2
	While: 1
*/
let i = 10;
while(i <= 20){ 
	console.log(i);
	i++;
}

// Do-while loop
/*
	Syntax:
		do{
			statement/s;
		}(condition)
*/

let number = Number(prompt("Give me a number"));
do{
	console.log("Do while: " + number);

	number += 1; // number = number + 1;
}while(number < 10)

let numA = Number(prompt("Assign a number"));
do{
	console.log("Do while: " + numA);
	numA -= 1;
}while(numA > 20)

// For Loop
/*
	Syntax:
		for(initialization; condition; finalExpression){
			statement/s;
		}
*/

for(let countNew = 0; countNew <= 20; countNew++){
	console.log(countNew);
}

let myString = "alex";

console.log("Total number of characters");
console.log(myString.length);

console.log("Accessing elements in String through index")
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);

console.log("Accessing indivitual elements through loops")
for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

let myName = "AlEx";
for(let i = 0; i < myName.length; i++){
	if(myName[i].toLowerCase() === 'a' ||
	   myName[i].toLowerCase() === 'e' ||
	   myName[i].toLowerCase() === 'i' ||
	   myName[i].toLowerCase() === 'o' ||
	   myName[i].toLowerCase() === 'u'){
	   console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

// Continue and Break statements

console.log("Continue and Break")
for(let count = 0; count <= 20; count++){
	if(count % 2 === 0){
		// This ignores all the statements located after the continue statements;
		continue;
	}
	console.log("Continue and Break: " + count);
	if(count > 10){
		// Number values after 10 will no longer be printed
		break;
	}
}

console.log("Continue and Break using strings");

let name = "alexandro";

for(let i = 0; name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}
	if(name[i] === "d"){
		break;
	}
}